# define _CRT_SECURE_NO_WARNINGS

# include <SDL.h>
# include <SDL_image.h>
# include <SDL_mixer.h>
# include <iostream>
# include <fstream>
# include <vector>
# include <sstream>
# include <queue>
using namespace std;
# undef main

# define _CRT_SECURE_NO_WARNINGS

# include <iostream>
# include <fstream>
# include <vector>
# include <sstream>
# include <queue>
using namespace std;

typedef struct Coordinate
{
	int x;
	int y;
}coord;

typedef struct Region
{
	coord c;
	int color;
	vector<coord> list;
} node;

vector<node> regions;
vector<node> regions_bak;
vector<int> choice;
vector<int **>boards;
ofstream out;
int DIMENSION;
int **board;
int **board_org;
int **mask;
int **board_bak;
int **regionMap;
int **graph;
int *distances;
int *bfsmap;
int furthestIndex;
int maxDistance;

void InitBoard(int dim)
{
	board = new int*[dim];
	mask = new int*[dim];
	board_bak = new int*[dim];
	regionMap = new int*[dim];
	board_org = new int*[dim];

	for (int i = 0; i < dim; i++)
	{
		board[i] = new int[dim];
		board_org[i] = new int[dim];
		mask[i] = new int[dim];
		board_bak[i] = new int[dim];
		regionMap[i] = new int[dim];
	}
}

void ClearMask()
{
	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
			mask[i][j] = 0;
	}
}

void PrintBoard()
{
	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			cout << board[i][j] << " ";
		}
		cout << endl;
	}
}

void PrintRegionOffset()
{
	for (int i = 0; i < regions.size(); i++)
	{
		cout << "Region " << i << ": ";
		for (int j = 0; j < regions[i].list.size(); j++)
			cout << "(" << regions[i].list[j].x << "," << regions[i].list[j].y << ")";
		cout << endl;
	}
}

void PrintRegionMap()
{
	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			cout << regionMap[i][j] << " ";
		}
		cout << endl;
	}
}

void PrintGraph()
{
	for (int i = 0; i < regions.size(); i++)
	{
		for (int j = 0; j < regions.size(); j++)
		{
			cout << graph[i][j] << " ";
		}
		cout << endl;
	}
}

void PrintBFS()
{
	for (int i = 0; i < regions.size(); i++)
		cout << i << " : " << distances[i] << endl;
}

void ExtractRegions(int x, int y, int z)
{
	mask[x][y] = 1;
	regionMap[x][y] = z;
	if (x > 0 && board[x][y] == board[x - 1][y] && mask[x - 1][y] != 1)
	{
		coord c;
		c.x = x - 1;
		c.y = y;
		regions[z].list.push_back(c);
		ExtractRegions(x - 1, y, z);
	}
	if (x < DIMENSION - 1 && board[x][y] == board[x + 1][y] && mask[x + 1][y] != 1)
	{
		coord c;
		c.x = x + 1;
		c.y = y;
		regions[z].list.push_back(c);
		ExtractRegions(x + 1, y, z);
	}
	if (y > 0 && board[x][y] == board[x][y - 1] && mask[x][y - 1] != 1)
	{
		coord c;
		c.x = x;
		c.y = y - 1;
		regions[z].list.push_back(c);
		ExtractRegions(x, y - 1, z);
	}
	if (y < DIMENSION - 1 && board[x][y] == board[x][y + 1] && mask[x][y + 1] != 1)
	{
		coord c;
		c.x = x;
		c.y = y + 1;
		regions[z].list.push_back(c);
		ExtractRegions(x, y + 1, z);
	}
}

void GenerateGraph()
{
	int prev = 0;
	for (int i = 0; i < DIMENSION; i++)
	{
		if (i > 0)
			prev = regionMap[i - 1][0];

		for (int j = 0; j < DIMENSION; j++)
		{
			if (prev != regionMap[i][j])
			{
				graph[prev][regionMap[i][j]] = 1;
				graph[regionMap[i][j]][prev] = 1;
				prev = regionMap[i][j];
			}
		}
	}
}

void MergeRegions(int x, int y)
{
	graph[x][y] = 0;
	graph[y][x] = 0;
	for (int i = 0; i < regions.size(); i++)
	{
		graph[x][i] = (graph[y][i] > graph[x][i]) ? graph[y][i] : graph[x][i];
		graph[y][i] = 0;
	}
}

void BFS(int source)
{
	queue<int> q;

	distances = new int[regions.size()];
	bfsmap = new int[regions.size()];

	for (int i = 0; i < regions.size(); i++)
	{
		distances[i] = -1;
		bfsmap[i] = 0;
	}

	distances[source] = 0;
	bfsmap[source] = 1;

	q.push(source);

	while (!q.empty())
	{
		int current = q.front();
		q.pop();

		for (int i = 0; i < regions.size(); i++)
		{
			if (graph[current][i] == 1 && bfsmap[i] == 0)
			{
				distances[i] = distances[current] + 1;
				q.push(i);
				bfsmap[i] = 1;
			}
		}

	}
}

coord FurthestDistance(int source)
{
	BFS(source);
	coord c;
	int max = 0;
	int index = -1;
	for (int i = 0; i < regions.size(); i++)
	{
		if (max < distances[i])
		{
			max = distances[i];
			index = i;
		}
	}
	c.x = index;
	c.y = max;
	return c;
}

int DistanceBetweenNodes(int source, int destination)
{
	BFS(source);

	return distances[destination];
}

void Dump(int color)
{
	out << "Color : " << color << endl;

	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			out << board[i][j] << " ";
		}
		out << endl;
	}
	out << "========================" << endl;
}

void RefreshBoard()
{
	int index = 0;
	ClearMask();
	regions.clear();
	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			if (mask[i][j] == 1)
				continue;
			node newNode;
			coord cd;
			cd.x = i;
			cd.y = j;
			newNode.c = cd;
			newNode.color = board[i][j];
			newNode.list.push_back(cd);
			regions.push_back(newNode);

			ExtractRegions(i, j, index++);
		}
	}

	graph = new int*[regions.size()];
	for (int i = 0; i < regions.size(); i++)
	{
		graph[i] = new int[regions.size()];
		for (int j = 0; j < regions.size(); j++)
			graph[i][j] = 0;
	}

	GenerateGraph();
}

void FloodIt(int x, int y, int col)
{
	mask[x][y] = 1;

	if (x > 0 && board[x - 1][y] == board[x][y] && mask[x - 1][y] != 1)
	{
		FloodIt(x - 1, y, col);
	}
	if (x < DIMENSION - 1 && board[x + 1][y] == board[x][y] && mask[x + 1][y] != 1)
	{
		FloodIt(x + 1, y, col);
	}
	if (y > 0 && board[x][y - 1] == board[x][y] && mask[x][y - 1] != 1)
	{
		FloodIt(x, y - 1, col);
	}
	if (y < DIMENSION - 1 && board[x][y + 1] == board[x][y] && mask[x][y + 1] != 1)
	{
		FloodIt(x, y + 1, col);
	}
	board[x][y] = col;
}

int LookAhead(int color)
{
	int result;
	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			board_bak[i][j] = board[i][j];
		}
	}

	regions_bak.clear();
	for (int i = 0; i < regions.size(); i++)
		regions_bak.push_back(regions[i]);

	ClearMask();
	FloodIt(0, 0, color);

	int index = 0;
	ClearMask();
	regions.clear();
	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			if (mask[i][j] == 1)
				continue;
			node newNode;
			coord cd;
			cd.x = i;
			cd.y = j;
			newNode.c = cd;
			newNode.color = board[i][j];
			newNode.list.push_back(cd);
			regions.push_back(newNode);

			ExtractRegions(i, j, index++);
		}
	}

	result = regions.size();

	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			board[i][j] = board_bak[i][j];
		}
	}

	regions.clear();
	for (int i = 0; i < regions_bak.size(); i++)
		regions.push_back(regions_bak[i]);

	return result;
}

void Heuristic1()
{
	int maxDist, nodeIndex, candidate;
	coord c = FurthestDistance(0);

	maxDist = c.y;
	nodeIndex = c.x;
	int maxChainLength = 0;

	while (maxDist != 0)
	{
		for (int i = 0; i < regions.size(); i++)
		{
			if (graph[0][i] == 1)
			{
				int temp = DistanceBetweenNodes(i, nodeIndex);
				if (temp + LookAhead(regions[i].color) <= maxDist + regions.size())
				{
					if (temp == maxDist && regions[i].list.size() >= maxChainLength)
					{
						maxDist = temp;
						candidate = i;
						maxChainLength = regions[i].list.size();
					}
					else if (temp < maxDist)
					{
						maxDist = temp;
						candidate = i;
						maxChainLength = regions[i].list.size();
					}

				}

			}
		}
		choice.push_back(regions[candidate].color);

		ClearMask();
		FloodIt(0, 0, regions[candidate].color);
		RefreshBoard();
		//Dump();

		c = FurthestDistance(0);

		maxDist = c.y;
		nodeIndex = c.x;
	}
}

void BackUp()
{
	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			board_bak[i][j] = board[i][j];
		}
	}
}

void Restore()
{
	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			board[i][j] = board_bak[i][j];
		}
	}
}

void PrintSolution()
{
	cout << "Total Moves : " << choice.size() << endl;
	for (int i = 0; i < choice.size(); i++)
		cout << choice[i] << " ";
	cout << endl;
}

int FetchResidualColoredDiagoanl()
{
	int color[6], count = 0;
	for (int i = 0; i < 6; i++)
		color[i] = 0;

	for (int i = 0; i < DIMENSION; i++)
	{
		if (color[board[i][i]] == 0)
		{
			color[board[i][i]] = 1;
			count++;
		}
	}
	return count;
}

int LookAhead2(int color)
{
	int result;
	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			board_bak[i][j] = board[i][j];
		}
	}

	regions_bak.clear();
	for (int i = 0; i < regions.size(); i++)
		regions_bak.push_back(regions[i]);

	ClearMask();
	FloodIt(0, 0, color);

	int index = 0;
	ClearMask();
	regions.clear();
	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			if (mask[i][j] == 1)
				continue;
			node newNode;
			coord cd;
			cd.x = i;
			cd.y = j;
			newNode.c = cd;
			newNode.color = board[i][j];
			newNode.list.push_back(cd);
			regions.push_back(newNode);

			ExtractRegions(i, j, index++);
		}
	}

	result = regions.size() + FetchResidualColoredDiagoanl();

	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			board[i][j] = board_bak[i][j];
		}
	}

	regions.clear();
	for (int i = 0; i < regions_bak.size(); i++)
		regions.push_back(regions_bak[i]);

	return result;
}

void Heuristic2()
{
	int min = 999;
	int minColor = 0;
	while (regions[0].list.size() != DIMENSION*DIMENSION)
	{
		for (int i = 0; i < regions.size(); i++)
		{
			if (graph[0][i] == 1)
			{
				int temp = LookAhead2(regions[i].color);
				if (temp <= min)
				{
					min = temp;
					minColor = regions[i].color;
				}
			}
		}

		choice.push_back(minColor);
		ClearMask();
		FloodIt(0, 0, minColor);

		RefreshBoard();

		//Dump();
	}
}

void DumpSolution()
{
	out << "Total moves : " << choice.size() << endl;

	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			board[i][j] = board_org[i][j];
			out << board[i][j] << " ";
		}
		out << endl;
	}
	out << "=====================" << endl;
	for (int i = 0; i < choice.size(); i++)
	{
		ClearMask();
		FloodIt(0, 0, choice[i]);
		Dump(choice[i]);
	}
}

int Draw(string msg)
{
	bool quit = false;
	SDL_Init(SDL_INIT_EVERYTHING);

	SDL_Window *window = NULL;

	window = SDL_CreateWindow("Flood It", 500, 175, 400, 400, SDL_WINDOW_SHOWN);

	SDL_Renderer *renderer = NULL;
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	SDL_Event *mainEvent = new SDL_Event();

	SDL_Rect white;
	white.x = 0;
	white.y = 0;
	white.w = 400;
	white.h = 400;

	SDL_Rect **grid;

	grid = new SDL_Rect*[DIMENSION];
	for (int i = 0; i < DIMENSION; i++)
		grid[i] = new SDL_Rect[DIMENSION];

	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			grid[i][j].x = (50 + j*(300 / DIMENSION));
			grid[i][j].y = (50 + i*(300 / DIMENSION));
			grid[i][j].w = 300 / DIMENSION;
			grid[i][j].h = 300 / DIMENSION;
		}
	}


	if (window == NULL)
	{
		return -1;
	}

	out << "Total moves : " << choice.size() << endl;

	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			board[i][j] = board_org[i][j];
			out << board[i][j] << " ";
		}
		out << endl;
	}
	out << "=====================" << endl;

	SDL_RenderClear(renderer);
	SDL_SetRenderDrawColor(renderer, 0xD0, 0xD4, 0xD0, 0xFF);
	SDL_RenderFillRect(renderer, &white);
	for (int i = 0; i < DIMENSION; i++)
	{
		for (int j = 0; j < DIMENSION; j++)
		{
			if (board[i][j] == 1)
				SDL_SetRenderDrawColor(renderer, 0xAA, 0xD6, 0x51, 0xFF);
			else if (board[i][j] == 2)
				SDL_SetRenderDrawColor(renderer, 0xF2, 0x99, 0x09, 0xFF);
			else if (board[i][j] == 3)
				SDL_SetRenderDrawColor(renderer, 0x36, 0x8D, 0xE3, 0xFF);
			else if (board[i][j] == 4)
				SDL_SetRenderDrawColor(renderer, 0xDB, 0x46, 0x4E, 0xFF);
			else if (board[i][j] == 5)
				SDL_SetRenderDrawColor(renderer, 0x55, 0xDB, 0x46, 0xFF);
			else if (board[i][j] == 6)
				SDL_SetRenderDrawColor(renderer, 0x89, 0x15, 0xD6, 0xFF);

			SDL_RenderFillRect(renderer, &grid[i][j]);

		}

	}
	SDL_RenderPresent(renderer);

	int count = 0;
	while (!quit && mainEvent->type != SDL_QUIT)
	{
		SDL_PollEvent(mainEvent);
		
		if (mainEvent->type == SDL_KEYDOWN && count < choice.size())
		{
			ClearMask();
			FloodIt(0, 0, choice[count]);
			Dump(choice[count]);
			SDL_RenderClear(renderer);
			SDL_SetRenderDrawColor(renderer, 0xD0, 0xD4, 0xD0, 0xFF);
			SDL_RenderFillRect(renderer, &white);

			for (int i = 0; i < DIMENSION; i++)
			{
				for (int j = 0; j < DIMENSION; j++)
				{
					if (board[i][j] == 1)
						SDL_SetRenderDrawColor(renderer, 0xAA, 0xD6, 0x51, 0xFF);
					else if (board[i][j] == 2)
						SDL_SetRenderDrawColor(renderer, 0xF2, 0x99, 0x09, 0xFF);
					else if (board[i][j] == 3)
						SDL_SetRenderDrawColor(renderer, 0x36, 0x8D, 0xE3, 0xFF);
					else if (board[i][j] == 4)
						SDL_SetRenderDrawColor(renderer, 0xDB, 0x46, 0x4E, 0xFF);
					else if (board[i][j] == 5)
						SDL_SetRenderDrawColor(renderer, 0x55, 0xDB, 0x46, 0xFF);
					else if (board[i][j] == 6)
						SDL_SetRenderDrawColor(renderer, 0x89, 0x15, 0xD6, 0xFF);

					SDL_RenderFillRect(renderer, &grid[i][j]);

				}

			}
			count++;
			SDL_RenderPresent(renderer);
		}

	}

	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	delete mainEvent;
}

int main(int argc, char *args[])
{
	ifstream in("input.txt");
	char s;
	int c = 1;
	string result = "output";

	while (true)
	{
		in >> s;
		DIMENSION = atoi(&s);
		InitBoard(DIMENSION);

		if (DIMENSION == 0)
			break;

		result = "heuristic1_";
		result.append(to_string(c++));
		result.append(".txt");
		out.open(result);

		for (int i = 0; i < DIMENSION; i++)
		{
			for (int j = 0; j < DIMENSION; j++)
			{
				in >> s;
				board[i][j] = atoi(&s);
				board_org[i][j] = atoi(&s);
			}
		}
		//PrintBoard();
		choice.clear();
		RefreshBoard();
		Heuristic1();
		PrintSolution();
		cout << endl;
		cout << "SIMULATION FOR HEURISTIC 1 SAMPLE " << (c - 1) << endl;
		Draw("HEURISTIC1");
		cout << endl;
		//DumpSolution();
		out.close();
		out.clear();

		result = "heuristic2_";
		result.append(to_string(c - 1));
		result.append(".txt");
		out.open(result);

		for (int i = 0; i < DIMENSION; i++)
		{
			for (int j = 0; j < DIMENSION; j++)
			{
				board[i][j] = board_org[i][j];
			}
		}

		choice.clear();
		RefreshBoard();
		Heuristic2();
		PrintSolution();
		cout << endl;
		cout << "SIMULATION FOR HEURISTIC 1 SAMPLE " << (c - 1) << endl;
		//DumpSolution();
		Draw("HEURISTIC2");
		cout << endl;
		out.close();
		out.clear();
	}

	cout << "ALL SOLUTION SAVED ON LOG..." << endl;
	getchar();
	getchar();
	return 0;
}